#!/bin/sh

# source: https://groups.google.com/a/zfsonlinux.org/d/msg/zfs-discuss/i09_VBXAyig/UWjPa23cQ0YJ

pids=""
tdir=/tmp/memtest.$$

function cleanup()
{
  [[ -n "${pids}" ]] && kill -9 ${pids} &> /dev/null || true

  # sleep a little!
  sleep 2
  umount ${tdir} &> /dev/null || true
  [[ -d ${tdir} ]] && /bin/rmdir ${tdir}
}

function run_md5sum()
{
  set +xv

  # in MiB
  msize=${1}

  filesize=$((msize/2))

  # seconds to run for
  ttime=${2}

  # file to create
  file=${3}
  echo
  echo "Creating file ${file} of size ${msize} MiB..."
  echo
  # creating pseudo-random data via openssl is faster than /dev/urandom
  pass="$(dd if=/dev/urandom bs=128 count=1 2>/dev/null | base64)"
  openssl enc -aes-256-cbc -pass pass:"$pass" -nosalt < /dev/zero 2>/dev/null |
         dd iflag=fullblock of=${file} count=${filesize} bs=1M
  echo "Calculatin md5sum of ${file}..."
  origmd5=$(md5sum ${file} | awk '{print $1}')
  cdt=$(date +%s)
  cdt=$((cdt+ttime))

  origfile=$file

  echo "Entering loop for ${file}..."

  # tight CPU loop
  while true
  do
    run=$((run+1))
    newfile=${origfile}.${run}

    # copy to new file, i.e. new location in RAM
    cat $file >$newfile

    oldfile=$file
    file=$newfile

    newmd5=$(md5sum ${file} | awk '{print $1}')

    rm $oldfile

    # trust that memory used by $origmd5 and $newmd5 is sane...:-)
    if [[ "${newmd5}" != "${origmd5}" ]]
    then
      echo
      echo "Failed md5sum on file ${file}, new=${newmd5}, org=${origmd5} ..." | tee -a /tmp/md5sum.err
      echo
      exit 1
    fi

    echo "${file} passed"

    dt=$(date +%s)
    if [[ ${dt} -gt ${cdt} ]]
    then
      echo "Thread ran all the way" | tee -a /tmp/md5sum.out
      exit 0
    fi

  done
}

# run verbose
#set -xv

# cleanup on exit
trap cleanup 1 2 3 6 15

# how long to run?
ttime=${1}
if [[ -z "${ttime}" ]]
then
  # run for 24 hours by default
  ttime=$((24*3600))
fi

# create a tmpfs with as much RAM you can spare. Drop caches first.
echo "Dropping caches..."
echo 3 > /proc/sys/vm/drop_caches
tmem=$(grep MemFree /proc/meminfo | awk '{print $2}')
tmem=$((tmem/1024))

echo
echo "Will create tmpfs of size ${tmem} MiB, and each thread will run for ${ttime} seconds"

mkdir ${tdir}
mount -t tmpfs -o size=$((tmem*1024*1024)) none ${tdir}

# spare 256MB for the system in single-user mode, for the programs
# spawned by this script and to avoid out of space tmpfs
tmem=$((tmem-256))

# spawn as many copies as the CPUs
ncpus=$(cat /proc/cpuinfo | grep processor | wc -l)
pcpumem=$((tmem/ncpus))
for i in $(seq 1 ${ncpus})
do
  run_md5sum $((pcpumem-2)) ${ttime} "${tdir}/${i}" &
  pids="${pids} $!"
done
sleep .5
echo
echo "Waiting for PIDs ${pids}..."
echo
wait ${pids}
cleanup
