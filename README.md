# md5-memtest
stress-test your RAM subsystem

Original from devsk on zfsonlinux mailing-list: https://groups.google.com/a/zfsonlinux.org/forum/#!msg/zfs-discuss/i09_VBXAyig/UWjPa23cQ0YJ after a suggestion from Gordan Bobic.

> memtest won't detect anything but permanently dead RAM. To properly
> stress-test your RAM subsystem, do something like the following: 
> 
> Create a tmpfs as big as you can spare in single-user mode. Fill it up 
> with n equally sized files generated from /dev/urandom where n is the 
> number of hardware CPU threads you have. Write a shell script that forks 
> n copies of itself, one for each file and gets md5sum of the files 
> repeatedly in a loop. Save the md5sum the first time, and then compare 
> against that value on each subsequent pass. Complain loudly on error. 
> 
> md5sum is very fast, so will stress the RAM I/O more than the CPU's 
> number-crunching capability. 
> 
> Leave running for 24 hours. If you get no errors (or ECC warnings if you 
> have ECC), your RAM is _probably_ OK. 
